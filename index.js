class Dinosaurus {
    static bobot = 100;
    constructor(name, type) {
        this.name = name;
        this.type = type;
    }

    introduce() {
        console.log("Spesies " + this.name + " adalah pemakan jenis " + this.type)
    }

    static isBobot(berat) {
        if(berat < 50) {
            return `Dinosaurus kecil dengan berat ${berat} ton`;
        } else {
            return `Dinosaurus besar dengan berat ${berat} ton`;
        }
    }
}

class Hewan extends Dinosaurus {
    constructor(name, type, turunan) {
        super(name, type);
        this.turunan = turunan;
    }

    introduce() {
        super.introduce();
        console.log(this.turunan + " adalah keturunan dari " + this.name);
    }
  
}

let mega = new Dinosaurus("Megalodon", "Karnivora");
mega.introduce();

let hiu = new Hewan("T-Rex", "Karnivora", "Ayam");
hiu.introduce();

console.log(Dinosaurus.isBobot(Dinosaurus.bobot));